#!/bin/bash

while getopts i:o: option
do
    case "${option}"
        in
        i) INFILE=${OPTARG};;
        o) OUTFILE=${OPTARG};;
    esac
done

if [ -z "${INFILE}" ]; then
    echo "Missing mandatory in file argument [-i]"
    exit 1
else
    if [ ! -f "${INFILE}" ]; then
        echo "In file argument [-i] value is not existing file: '${INFILE}'"
        exit 1
    fi
fi
if [ -z "${OUTFILE}" ]; then
    OUTFILE=${INFILE}
fi

curl -s -f -H "Content-Type: application/octet-stream" -H "X-API-Key: ${VEIT_CI_SERVICE_API_KEY}" --data-binary @${INFILE} -o ${OUTFILE} -X POST ${VEIT_CI_SERVICE_BASE_URL}/sign
