﻿using System;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Transfer;

namespace Veit.Diagnostics.AWS
{
#pragma warning disable CA1724 // Type names should not match namespaces
    public static class S3
#pragma warning restore CA1724 // Type names should not match namespaces
    {
        private const string BUCKET_NAME =
#if !DEBUG
         "diagdata.veit.cz";
#else
         "diagdata-stg.veit.cz";
#endif

        private const string UP_ACCESS_KEY = "AKIAJLWLNJRYZMOSMFGQ";
        private const string UP_SECRET_KEY = "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";
        private const string ROOT_FOLDER = "upload";


        public static void Put(string filePath, string subKey)
        {
            using (var fileTransferUtility = CreateTransferUtility())
            {
                fileTransferUtility.Upload(filePath, BUCKET_NAME, FormatKeyName(filePath, subKey));
            }
        }

        public async static Task PutAsync(string filePath, string subKey)
        {
            using (var fileTransferUtility = CreateTransferUtility())
            {
                await fileTransferUtility.UploadAsync(filePath, BUCKET_NAME, FormatKeyName(filePath, subKey));
            }
        }


        /// <summary>
        /// Send data to S3.
        /// </summary>
        /// <param name="stream">Data stream</param>
        /// <param name="sourceName">Name of the source file. Serves for getting file extension</param>
        /// <param name="subKey">Name that will be part of output file name.</param>
        public static void Put(Stream stream, string sourceName, string subKey)
        {
            using (var fileTransferUtility = CreateTransferUtility())
            {
                fileTransferUtility.Upload(stream, BUCKET_NAME, FormatKeyName(sourceName, subKey));
            }
        }

        public async static Task PutAsync(Stream stream, string sourceName, string subKey)
        {
            using (var fileTransferUtility = CreateTransferUtility())
            {
                await fileTransferUtility.UploadAsync(stream, BUCKET_NAME, FormatKeyName(sourceName, subKey));
            }
        }


        #region Private helpers

        private static TransferUtility CreateTransferUtility()
        {
            var creds = new BasicAWSCredentials(UP_ACCESS_KEY, UP_SECRET_KEY);
            return new TransferUtility(new AmazonS3Client(creds, RegionEndpoint.EUCentral1));
        }

        private static string FormatKeyName(string file, string name)
        {
            var timeStamp = DateTime.Now.ToString("O", CultureInfo.InvariantCulture);  //to ISO 8601
            return $"{ROOT_FOLDER}/{name}_{timeStamp}{Path.GetExtension(file)??string.Empty}";
        }

        #endregion
    }
}
